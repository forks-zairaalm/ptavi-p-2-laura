#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math

class Compute():

    count = 0  # Creamos una variable que empiece en cero a contar, porque puede o no usar alguna de las funciones y nos tendría que devolver 0. Por eso no se empieza en 1

    def __init__(self):
        self.default = 2

    def power(self, num, exp=2):
        self.count += 1  # Añadimos la variable count que sume uno cada vez que se use cada función
        return num ** exp

    def log(self, num, base=2):
        self.count += 1
        return math.log(num, base)

    def set_def(self,num2):
        self.count += 1
        if num2 == None:
            return (self.default)
        if num2 <= 0:
            raise (ValueError('Error: third argument should be a number'))

    def get_def(self):
        self.count += 1
        return (self.default)