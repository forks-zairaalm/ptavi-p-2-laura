#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math
import sys

class Compute(): # Creamos la nueva clase Compute
      pass       # Esta línea significa que al estar vacío Compute(), que de momento no lo tome como un error si está vacio, que se rellenará luego, que "pase"
      def __init__(self): # Es el primer método definido para la clase, inicializador
          self.default = 2  # Esta instancia es una variable, aunque en este caso le hemos dado un valor fijo

      def power(self, num, exp=2):
          return num ** exp

      def log(self, num, base=2):
          return math.log(num, base)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = 2
    else:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    objecto = Compute()  # Hacemos una instancia al objeto dándole un nombre 'objeto'

    if sys.argv[1] == "power":
        result = objecto.power(num,num2)  # Debemos cambiarle el valor de result, ya que llamamos a la función a través del objeto
    elif sys.argv[1] == "log":
        result = objecto.log(num,num2)
    else:
        sys.exit('Operand should be power or log')

    print(result)